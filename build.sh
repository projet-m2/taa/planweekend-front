#!/bin/sh
echo "### building angular app ###\n"
ng build --prod --aot
echo "### bilding Docker image ###\n"
docker build -t registry.gitlab.com/projet-m2/taa/planweekend-front -f Dockerfile ./dist
