import { NgModule, LOCALE_ID } from '@angular/core';
import { DatePipe, registerLocaleData } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { Title } from '@angular/platform-browser';
import locale from '@angular/common/locales/en';
import { NgxWebstorageModule } from 'ngx-webstorage';
import { HasAnyAuthorityDirective } from './auth/directives/has-any-authority.directive';

@NgModule({
  imports: [HttpClientModule, NgxWebstorageModule.forRoot({ prefix: 'taa', separator: '-' })],
  exports: [],
  declarations: [HasAnyAuthorityDirective],
  providers: [
    Title,
    {
      provide: LOCALE_ID,
      useValue: 'en'
    },
    DatePipe
  ]
})
export class PlanWeekEndCoreModule {
  constructor() {
    registerLocaleData(locale);
  }
}
