export const authorities = {
    admin: 'ROLE_ADMIN',
    user: 'ROLE_USER'
}