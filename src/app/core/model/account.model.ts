export class Account {
  constructor(
    public email: string,
    public authorities?: string[],
  ) {}
}
