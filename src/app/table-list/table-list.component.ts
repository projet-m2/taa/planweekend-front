import {Component, OnInit} from '@angular/core';
import {Activity} from '../entities/model/activity.model';
import {ActivityService} from '../entities/activity/activity.service';
import {LocalisationService} from '../entities/localisation/localisation.service';
import {Localisation} from '../entities/model/localisation.model';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
    selector: 'app-table-list',
    templateUrl: './table-list.component.html',
    styleUrls: ['./table-list.component.css']
})
export class TableListComponent implements OnInit {

    activities: Activity[];
    localisations: Localisation[];
    selectedLocalisation;

    constructor(
        private activityService: ActivityService,
        private localisationService: LocalisationService,
        public snackBar: MatSnackBar
    ) {
        this.activities = [];
        this.localisations = [];
    }

    ngOnInit(): void {
        this.activityService.getAllActivities().subscribe((activities: Activity[]) => {
            this.activities = activities;
        })
        this.localisationService.getAllLocalisation().subscribe((localisations: Localisation[]) => {
            this.localisations = localisations;
        })
    }

    updateActivity(activityId: number) {
        console.log(activityId + '&' + this.selectedLocalisation);
        this.activityService.updateActivity(activityId, this.selectedLocalisation)
            .toPromise()
            .then(() => {
                this.snackBar.open('Localisation update successfully', 'Close', {
                    duration: 1000,
                });
            }).catch(reason => {
            this.snackBar.open(`Failed to update localisation: ${reason}`, 'Close', {
                duration: 1000,
            });
        });
    }

}
