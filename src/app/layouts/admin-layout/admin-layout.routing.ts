import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddComponent } from '../../add/add.component';
import { DashboardComponent } from '../../dashboard/dashboard.component';
import { TableListComponent } from '../../table-list/table-list.component';
import { UserProfileComponent } from '../../user-profile/user-profile.component';


export const AdminLayoutRoutes: Routes = [
    { path: '', redirectTo: 'overview'},
    { path: 'overview', component: DashboardComponent },
    { path: 'user-profile', component: UserProfileComponent },
    { path: 'table-list', component: TableListComponent },
    { path: 'add', component: AddComponent},
];

@NgModule({
    imports: [
        RouterModule.forChild(AdminLayoutRoutes)
    ],
    exports: [],
})
export class AdminLayoutRoutingModule {
}