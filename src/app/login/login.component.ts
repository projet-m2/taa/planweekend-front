import { Component, OnInit } from '@angular/core';
import { LoginService } from 'app/core/auth/services/login.service';
import { Router } from '@angular/router';
import { AccountService } from 'app/core/auth/services/account.service';
import { Account } from 'app/core/model/account.model';
import { LoginModel } from 'app/core/model/login.model';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public isRegister = false;
  public user_login: string;
  public user_password: string;
  public user_rememberMe: boolean;

  constructor(
    public loginService: LoginService,
    public accountService: AccountService,
    public router: Router,
    public snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    console.log('debug', 'login init');
    console.log('debug', 'is authenticated', this.accountService.isAuthenticated());
    if (this.accountService.isAuthenticated())
      this.router.navigate['dashboard'];
  }

  setIsRegister(value: boolean) {
    this.isRegister = value;
  }

  login() {
    console.log('debug', 'login', this.user_login, this.user_password);
    this.loginService.login({ email: this.user_login, password: this.user_password, rememberMe: this.user_rememberMe })
      .then(() => this.router.navigate(['dashboard']))
      .catch(reason => this.snackBar.open(`Failed to log in`, 'Close', {
        duration: 1000,
      }));
  }

  register() {
    console.log('debug', 'sign-in', this.user_login, this.user_password);
    this.accountService.register(new LoginModel(this.user_login, this.user_password))
      .toPromise()
      .then(() => {
        this.isRegister = false;
        this.snackBar.open('Registered successfully', 'Close', {
          duration: 1000,
        });
      }).catch(reason => {
        this.isRegister = false;
        this.snackBar.open(`Failed to register: ${reason}`, 'Close', {
          duration: 1000,
        });
      })
  }
}
