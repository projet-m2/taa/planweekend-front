import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';
import {ILocalisation, Localisation} from '../model/localisation.model';

@Injectable({
    providedIn: 'root'
})
export class LocalisationService {

    constructor(protected http: HttpClient) {
    }

    getAllLocalisation(): Observable<ILocalisation[]> {
        return this.http.get<ILocalisation[]>(environment.backendUri + 'api/localisations');
    }

    createLocalisation(localisation: Localisation): Observable<ILocalisation> {
        return this.http.post<ILocalisation>(environment.backendUri + 'api/localisation', localisation);
    }
}
