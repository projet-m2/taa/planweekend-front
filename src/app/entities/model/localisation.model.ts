import {IActivity} from 'app/entities/model/activity.model';

export interface ILocalisation {
    name: string;
    id?: number;
    longitude?: number;
    latitude?: number;
    activities?: IActivity[];
}

export class Localisation implements ILocalisation {
    constructor(
        public name: string,
        public id?: number,
        public longitude?: number,
        public latitude?: number,
        public activities?: IActivity[]) {
    }
}
