export interface IMeteoRestriction {
    id?: number;
}

export class MeteoRestriction implements IMeteoRestriction {
    constructor(public id?: number) {
    }
}
