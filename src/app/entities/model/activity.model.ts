import {ILocalisation} from 'app/entities/model/localisation.model';
import {IMeteoRestriction} from './meteoRestriction.model';

export interface IActivity {
    type: string;
    id?: number;
    localisations?: ILocalisation[];
    restrictions?: IMeteoRestriction[];
}

export class Activity implements IActivity {
    constructor(
        public type: string,
        public id?: number,
        public localisations?: ILocalisation[],
        public restrictions?: IMeteoRestriction[],
    ) {
    }

    getLocalisations() {
        return this.localisations.forEach(l => l.name);
    }
}
