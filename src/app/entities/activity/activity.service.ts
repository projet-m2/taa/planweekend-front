import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Activity, IActivity} from '../model/activity.model';
import {environment} from '../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class ActivityService {

    constructor(protected http: HttpClient) {
    }

    getAllActivities(): Observable<IActivity[]> {
        return this.http.get<IActivity[]>(environment.backendUri + 'api/activities');
    }

    createActivity(activity: Activity): Observable<IActivity> {
        return this.http.post<IActivity>(environment.backendUri + 'api/activity', activity);
    }

    updateActivity(activityId: number, localisationId: number): Observable<IActivity> {
        return this.http.put<IActivity>(environment.backendUri + 'api/activity/' + activityId + '/localisation/' + localisationId, null);
    }

}
