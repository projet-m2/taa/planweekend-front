import {Component, OnInit} from '@angular/core';
import {Activity} from '../entities/model/activity.model';
import {Localisation} from '../entities/model/localisation.model';
import {LocalisationService} from '../entities/localisation/localisation.service';
import {ActivityService} from '../entities/activity/activity.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Local} from 'protractor/built/driverProviders';

@Component({
    selector: 'app-add',
    templateUrl: './add.component.html',
    styleUrls: ['./add.component.scss']
})
export class AddComponent implements OnInit {

    activity;
    localisation;
    selectedLocalisation;
    localisations: Localisation[];

    constructor(private localisationService: LocalisationService,
                private activityService: ActivityService,
                public snackBar: MatSnackBar) {
        this.localisations = [];
    }

    ngOnInit(): void {
        this.localisationService.getAllLocalisation().subscribe((localisations: Localisation[]) => {
            this.localisations = localisations;
        })
    }

    createActivity() {
        console.log(this.activity);
        this.activityService.createActivity(new Activity(this.activity))
            .toPromise()
            .then(() => {
                this.activity = '';
                this.snackBar.open('Activity create successfully', 'Close', {
                    duration: 1000,
                });
            }).catch(reason => {
            this.snackBar.open(`Failed to create activity: ${reason}`, 'Close', {
                duration: 1000,
            });
        });
    }

    createLocalisation() {
        console.log(this.localisation);
        this.localisationService.createLocalisation(new Localisation(this.localisation))
            .toPromise()
            .then(() => {
                this.localisation = '';
                this.snackBar.open('Localisation create successfully', 'Close', {
                    duration: 1000,
                });
            }).catch(reason => {
            this.snackBar.open(`Failed to create localisation: ${reason}`, 'Close', {
                duration: 1000,
            });
        });
    }
}
