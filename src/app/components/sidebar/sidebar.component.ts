import { Component, OnInit } from '@angular/core';
import { LoginService } from 'app/core/auth/services/login.service';
import { Router } from '@angular/router';

declare const $: any;
declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}
export const ROUTES: RouteInfo[] = [
    {path: 'overview', title: 'Dashboard', icon: 'dashboard', class: ''},
    {path: 'user-profile', title: 'User Profile', icon: 'person', class: ''},
    {path: 'table-list', title: 'Activities List', icon: 'content_paste', class: ''},
    {path: 'add', title: 'Add', icon: 'add', class: ''}
];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  menuItems: any[];

  constructor(
    private loginService: LoginService,
    private router: Router
  ) { }

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
  }
  isMobileMenu() {
      if ($(window).width() > 991) {
          return false;
      }
      return true;
  };

  logout(){
    this.loginService.logout();
    this.router.navigateByUrl('/login');
  }
}
