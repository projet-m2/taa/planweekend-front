import {Component, OnInit} from '@angular/core';
import {Activity} from '../entities/model/activity.model';
import {ActivityService} from '../entities/activity/activity.service';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

    activities: Activity[];

    constructor(private activityService: ActivityService) {
        this.activities = [];
    }

    ngOnInit(): void {
        this.activityService.getAllActivities().subscribe((activities: Activity[]) => {
            this.activities = activities;
        })
    }

}
