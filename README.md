# Plannification de week-ends FRONT

## Equipe M2 ILA

 - Victor HARABARI
 - Nathanaël TOUCHARD
 
## Installation du front en local

```bash
npm install
```

```bash
ng build --prod --aot
```

```bash
docker-compose up
```
